import sublime
import sublime_plugin

def get_current_indentation(view, point):
    pt = view.line(point).begin()
    result = ''
    while True:
        c = view.substr(pt)
        if c == " " or c == "\t":
            pt += 1
            result += c
        else:
            break

    return result

class WrapHtmlInPhpCommentCommand(sublime_plugin.TextCommand):
    def run(self, edit, selection_start = '<?php /*', selection_end = '*/ ?>'):
        sel = self.view.sel()
        for r in tuple(reversed(sel)):

            region_end = self.view.line(r.end()).end()
            if self.view.classify(r.end()) & sublime.CLASS_LINE_START:
                region_end = r.end() - 1

            region = sublime.Region(self.view.line(r.begin()).begin(), region_end)
            indent = get_current_indentation(self.view, region.begin())
            insert_start = indent + selection_start + "\n"
            insert_end = '\n' + indent + selection_end

            self.view.insert(edit, region.begin(), insert_start)
            self.view.insert(edit, region.end() + len(insert_start), insert_end)

            indent_region = sublime.Region(region.begin() + len(insert_start) + 1, region.end() + len(insert_start))
            num_lines_indented = len(self.view.lines(indent_region))

            self.view.sel().clear()
            self.view.sel().add(indent_region)

            sel.clear()
            settings = self.view.settings()
            pt = region.begin() + len(indent)
            sel.add(sublime.Region(pt, pt))
