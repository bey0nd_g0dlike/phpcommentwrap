# Sublime PHP Comment Wrap

A snippet to wrap your HTML code in PHP comments. Very useful for WordPress developers when there's a lot of HTML code in PHP files.

Plugin function is limited to only wrapping the HTML and JS code inside PHP files.

## Quickstart
=======

1. Install Via [Package Control](https://packagecontrol.io) `PHP Comment Wrap`
2. Select some text.
3. Use the default key bindings 'ctrl+alt+c' to wrap the selection in php comments
4. Enjoy!

## Installation
=======

### via PackageControl
If you have [PackageControl](http://wbond.net/sublime_packages/package_control) installed, you can use it to install the package.

Just type `cmd-shift-p`/`ctrl-shift-p` to bring up the command pallete and pick `Package Control: Install Package` from the dropdown, search and select the package there and you're all set.

### Manually
=======

You can clone the repo in your `/Packages` (*Preferences -> Browse Packages...*) folder and start using/hacking it.

    cd ~/path/to/Packages
    git clone https://bitbucket.org/bey0nd_g0dlike/phpcommentwrap PHPCommentWrap

## Options
=======

__`selection_start` & `selection_end`:__ These options allow you to modify the kind of selection wrapping the plugin will insert.<br>

    {
    	"keys": ["ctrl+alt+c"],
    	"command": "wrap_html_in_php_comment",
    	"args": {
    		"selection_start": "<!--",
    		"selection_end": "-->"
    	},
    	"context":
		[
			{
				"key": "selector",
				"operator": "equal",
				"operand": "text.html"
			},
			{
				"key": "selector",
				"operator": "not_equal",
				"operand": "source.php"
			}
		]
	}

## Issues
=======

This is the first version, so feel free to comment or send requests at:
http://bitbucket.org/bey0nd_g0dlike/sublime-text-php-comment-wrap/issues
